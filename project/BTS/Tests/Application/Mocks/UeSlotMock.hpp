#pragma once

#include <gmock/gmock.h>
#include "UeConnection/UeSlot.hpp"
#include "Printers/UeSlotPrint.hpp"

namespace bts
{

class IUeSlotImplMock : public UeSlot::IImpl
{
public:
    IUeSlotImplMock();
    ~IUeSlotImplMock() override;

    MOCK_METHOD2(sendMessage, bool(BinaryMessage message, PhoneNumber to));
    MOCK_METHOD1(attach, UeSlot::IImplPtr(PhoneNumber phone));
    MOCK_CONST_METHOD0(isAttached, bool());
    MOCK_CONST_METHOD0(getPhoneNumber, PhoneNumber());
    MOCK_METHOD0(remove, void());
};


}
