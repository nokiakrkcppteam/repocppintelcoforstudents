#include <gtest/gtest.h>

#include "Application.hpp"

#include "Mocks/ILoggerMock.hpp"
#include "Messages/BtsId.hpp"

#include <memory>

namespace ue
{

class ApplicationTestSuite : public ::testing::Test
{
protected:
    ::testing::NiceMock<common::ILoggerMock> loggerMock{};
    Application objectUnderTest;

};

}
